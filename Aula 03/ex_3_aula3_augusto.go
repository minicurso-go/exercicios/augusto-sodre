package main

import "fmt"

func main() {
	var num int
	fmt.Println("Tabuada!")
	fmt.Print("Tabuada do número: ")
	fmt.Scan(&num)
	for i := 1; i <= 10; i++ {
		fmt.Printf("\n%d X %d = %d\n", i, num, num*i)
	}

}
