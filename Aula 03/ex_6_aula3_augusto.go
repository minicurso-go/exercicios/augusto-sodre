package main

import "fmt"

func main() {
	var resp string
	var num, num_maior int
	fmt.Println("--------------------")
	fmt.Println("Vamos ver qual o maior número! Me dê quantos números quiser e lhe direi qual o maior!")
	for resp != "n" {
		fmt.Print("Dê-me um número inteiro: ")
		fmt.Scan(&num)
		if num >= num_maior {
			num_maior = num
		}
		if num == 0 {
			break
		}
		fmt.Print("E agora, deseja continuar? [s/n]: ")
		fmt.Scan(&resp)
	}
	fmt.Printf("\n\nO maior número é: %d\n", num_maior)
}
