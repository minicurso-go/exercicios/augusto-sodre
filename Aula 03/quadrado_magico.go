package main

import "fmt"

func check(l0 int, l1 int, l2 int, c0 int, c1 int, c2 int, d1 int, d2 int) bool {
	if (l0 == l1) && (l1 == l2) && (l0 == c0) && (c0 == c1) && (c1 == c2) && (d1 == d2) && (d1 == l0) {
		return true
	}
	return false
}

func main() {
	var matriz [3][3]int
	var linha0, linha1, linha2, coluna0, coluna1, coluna2, diagonal1, diagonal2 int
	for l := 0; l < 3; l++ {
		for c := 0; c < 3; c++ {
			fmt.Printf("Digite a linha %d e coluna %d: ", l, c)
			fmt.Scan(&matriz[l][c])
		}
	}
	for l := 0; l < 3; l++ {
		for c := 0; c < 3; c++ {
			if l == 0 {
				linha0 += matriz[l][c]
				if c == 0 {
					diagonal1 += matriz[l][c]

				}
			} else if l == 1 {
				linha1 += matriz[l][c]
				if c == 1 {
					diagonal1 += matriz[l][c]
					diagonal2 += matriz[l][c]
				}
			} else if l == 2 {
				linha2 += matriz[l][c]
				if c == 2 {
					diagonal1 += matriz[l][c]
				}
			}
		}
	}

	for c := 0; c < 3; c++ {
		for l := 0; l < 3; l++ {
			if c == 0 {
				coluna0 += matriz[l][c]
				if l == 2 {
					diagonal2 += matriz[l][c]
				}
			} else if c == 1 {
				coluna1 += matriz[l][c]
			} else if c == 2 {
				coluna2 += matriz[l][c]
				if l == 0 {
					diagonal2 += matriz[l][c]
				}
			}
		}

	}
	fmt.Println("Seu quadrado:", matriz)
	fmt.Println("Seu quadrado é mágico: ", check(linha0, linha1, linha2, coluna0, coluna1, coluna2, diagonal1, diagonal2))
}
