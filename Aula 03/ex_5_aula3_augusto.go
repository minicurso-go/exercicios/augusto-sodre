package main

import "fmt"

func main() {
	var num int
	fmt.Print("Dê-me um número inteiro positivo: ")
	fmt.Scan(&num)
	fmt.Printf("\nDivisores do número %d:\n", num)
	for i := num; i >= 1; i-- {
		if num%i == 0 {
			fmt.Println(i)
		}
	}

}
