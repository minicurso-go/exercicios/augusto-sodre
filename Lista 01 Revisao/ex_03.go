package main

import "fmt"

func main() {
	elements := [4]float64{4, 5, 6, 7}
	fmt.Println("Array utilizada:", elements)
	elementsProduct := 1.0
	for i := 0; i < len(elements); i++ {
		elementsProduct = elementsProduct * elements[i]
	}
	fmt.Println("Produto dos elementos da array: ", elementsProduct)
}
