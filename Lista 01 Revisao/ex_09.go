package main

import "fmt"

func main() {
	elements := []int{10, -100, 130, 140, 150, 160, 170, 180, 9999, 1100}
	numMax := -200000000
	numMin := 200000000
	for i := 0; i < len(elements); i++ {
		if elements[i] <= numMin {
			numMin = elements[i]
		} else if elements[i] >= numMax {
			numMax = elements[i]
		}
	}
	fmt.Println("Lista original:", elements)
	fmt.Println("Número máximo:", numMax)
	fmt.Println("Número mínimo:", numMin)
}
