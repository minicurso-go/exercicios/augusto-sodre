package main

import "fmt"

func main() {
	elements := [3]int{2, 8, 6}
	elementSum := 0
	for i := 0; i < len(elements); i++ {
		elementSum = elementSum + elements[i]
	}
	fmt.Println("Soma dos elementos:", elementSum)
}
