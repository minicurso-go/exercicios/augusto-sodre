package main

import "fmt"

func main() {
	var elements1 []int
	var elements2 []int
	var elementsResult []int
	var length int
	fmt.Println("Somador de elementos!")
	fmt.Print("Quantos elementos terão as listas: ")
	fmt.Scan(&length)
	elements1 = make([]int, length)
	elements2 = make([]int, length)
	elementsResult = make([]int, length)
	fmt.Println("-> Primeira lista")
	for i := 0; i < length; i++ {
		fmt.Printf("Dê-me o elemento da posição %d: ", i)
		fmt.Scan(&elements1[i])
	}
	fmt.Println("-> Segunda lista:")
	for i := 0; i < length; i++ {
		fmt.Printf("Dê-me o elemento da posição %d: ", i)
		fmt.Scan(&elements2[i])
	}
	for i := 0; i < length; i++ {
		elementsResult[i] = elements1[i] + elements2[i]
	}
	fmt.Println("-> Resultados:")
	fmt.Println("Lista 1:", elements1)
	fmt.Println("Lista 2:", elements2)
	fmt.Println("Result :", elementsResult)

}
