package main

import "fmt"

func main() {
	var elements []int
	var num int
	var canAdd bool
	elements = make([]int, 5)
	elements = []int{12, 17, 22, 27, 32}
	canAdd = true
	fmt.Println("Lista de números:", elements)
	fmt.Print("Dê-me um número para adicionar a lista: ")
	fmt.Scan(&num)
	for i := 0; i < len(elements); i++ {
		if num == elements[i] {
			canAdd = false
		}
	}
	if canAdd == true {
		elements = append(elements, num)
		fmt.Println("Lista:", elements)
	} else {
		fmt.Println("O número digitado já está na lista!")
	}
}
