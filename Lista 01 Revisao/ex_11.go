package main

import "fmt"

func main() {
	var elements [7]int
	var firstNum, lastNum int
	fmt.Println("Lista original:", elements)
	fmt.Print("Dê-me um número para ser o primeiro da lista: ")
	fmt.Scan(&firstNum)
	fmt.Print("Dê-me um número para ser o último da lista: ")
	fmt.Scan(&lastNum)
	elements[0] = firstNum
	elements[(len(elements) - 1)] = lastNum
	fmt.Println("Lista final:", elements)
}
