package main

import "fmt"

func main() {
	elements := []int{8, 7, 6, 5, 4, 3, 2, 1}
	var num1, num2, pos1, pos2 int
	fmt.Println("Troca de números!")
	fmt.Println("Lista original:", elements)
	fmt.Print("Número a ser trocado: ")
	fmt.Scan(&num1)
	fmt.Print("Segundo número a ser trocado: ")
	fmt.Scan(&num2)
	for i := 0; i < len(elements); i++ {
		if elements[i] == num1 {
			pos1 = i
		} else if elements[i] == num2 {
			pos2 = i
		}
	}
	elements[pos1] = num2
	elements[pos2] = num1
	fmt.Println("Nova lista:", elements)

}
