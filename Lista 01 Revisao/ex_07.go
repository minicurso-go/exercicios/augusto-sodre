package main

import "fmt"

func main() {
	elements := []string{"a", "b", "c", "d", "a", "b", "c", "d"}
	var num string
	fmt.Println("Lista original:", elements)
	fmt.Print("Dê-me um valor para retirar da lista: ")
	fmt.Scan(&num)
	i := 0
	for i < len(elements) {
		if num == elements[i] {
			elements = append(elements[:i], elements[(i+1):]...)
			i = 0
		} else {
			i++
		}
	}
	fmt.Println("Lista final:", elements)
}
