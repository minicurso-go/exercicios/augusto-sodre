package main

import "fmt"

func main() {
	elements := []int{1, 2, 3, 4, 5}
	fmt.Println("Array original:", elements)
	elements = append(elements[:2], elements[3:]...)
	fmt.Println("Retirando-se o terceiro elemento:", elements)
}
