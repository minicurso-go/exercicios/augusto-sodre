package main

import "fmt"

func check(array []int, length int) bool {
	var pastElement int
	pastElement = array[0]
	for c := 1; c < length; c++ {
		if array[c] < pastElement {
			return false
		}
	}
	return true
}

func main() {
	var elements []int
	var length int
	fmt.Println("Verificador de ordem crescente!")
	fmt.Print("Quantos elementos tem sua lista: ")
	fmt.Scan(&length)
	elements = make([]int, length)
	for i := 0; i < length; i++ {
		fmt.Printf("Elemento da posição %d: ", i)
		fmt.Scan(&elements[i])
	}
	fmt.Println("Lista a ser analisada:", elements)
	fmt.Println("Array crescente:", check(elements, length))
}
