package main

import "fmt"

func main() {
	var elements []int
	var numElements int
	fmt.Println("Criador de arrays!")
	fmt.Print("Quantidade de elementos a serem colocados: ")
	fmt.Scan(&numElements)
	elements = make([]int, numElements)
	for i := 0; i < numElements; i++ {
		fmt.Print("Elemento a ser inserido: ")
		fmt.Scan(&elements[i])
	}
	fmt.Println("Array final:", elements)
}
