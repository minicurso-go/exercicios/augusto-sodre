package main

import "fmt"

func main() {
	var elements []float64
	var num float64
	elements = make([]float64, 6)
	fmt.Print("Dê-me um valor para adicionar em toda a lista: ")
	fmt.Scan(&num)
	for i := 0; i < len(elements); i++ {
		elements[i] = num
	}
	fmt.Println("Lista final:", elements)
}
