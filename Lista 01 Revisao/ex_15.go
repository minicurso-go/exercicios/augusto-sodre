package main

import "fmt"

func main() {
	elements := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	var sumOfElements int
	fmt.Println("Lista original:", elements)
	for i := 0; i < len(elements); i += 2 {
		sumOfElements += elements[i]
	}
	fmt.Println("Soma dos números nas posições pares:", sumOfElements)
}
