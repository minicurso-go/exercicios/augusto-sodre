package main

import "fmt"

func main() {
	elements := []float64{1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5}
	var newElements []float64
	fmt.Println("Lista original:", elements)
	for i := 0; i < len(elements); i++ {
		if elements[i] > 5 {
			newElements = append(newElements, elements[i])
		}
	}
	fmt.Println("Elementos maiores que cinco:", newElements)
}
