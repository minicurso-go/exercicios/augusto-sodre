package main

import "fmt"

func main() {
	elements := [10]int{12, 15, 16, 17, 28, 43, 45, 56, 88, 96}
	var searchElement int
	fmt.Println("Buscador!")
	fmt.Print("Dê-me um valor para buscar no vetor: ")
	fmt.Scan(&searchElement)
	for i := 0; i < len(elements); i++ {
		if elements[i] == searchElement {
			fmt.Println("Eu o achei! Está na posição", i)
			break
		} else if i == (len(elements) - 1) {
			fmt.Println("Ele não está na lista!")
		}
	}
}
