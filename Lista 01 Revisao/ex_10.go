package main

import (
	"fmt"
)

func main() {
	elements := [5]int{15, 9, 13, 12, 11}
	var threeMultiples []int
	fmt.Println("Lista original:", elements)
	for i := 0; i < len(elements); i++ {
		if elements[i]%3 == 0 {
			threeMultiples = append(threeMultiples, elements[i])
		}
	}
	fmt.Println("Lista de múltiplos de 3:", threeMultiples)

}
