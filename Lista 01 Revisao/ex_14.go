package main

import "fmt"

func main() {
	elements := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	var newElements []int
	fmt.Println("Lista original:", elements)
	for i := 0; i < len(elements); i++ {
		if elements[i]%2 == 0 {
			newElements = append(newElements, elements[i])
		}
	}
	fmt.Println("Lista de pares:", newElements)
}
