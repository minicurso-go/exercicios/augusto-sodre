# Augusto Sodré - 1° semestre

## Project Name
Go - Noob to Pro 👍

## Description
Just playing around and understanding how Go and Git work together

## License
Educational purposes

## Project status
Constantly being uploaded (I guess)

## Colinha de comandos hehe:

### Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git clone https://gitlab.com/minicurso-go/exercicios/augusto-sodre.git
git branch -M main
git push -uf origin main
```

***
