package main

import "fmt"

func main() {
	var n1, n2, n3, n4, media float32
	fmt.Println("Calculadora de média!")
	fmt.Print("Dê-me a nota 1: ")
	fmt.Scan(&n1)
	fmt.Print("Dê-me a nota 2: ")
	fmt.Scan(&n2)
	fmt.Print("Dê-me a nota 3: ")
	fmt.Scan(&n3)
	fmt.Print("Dê-me a nota 4: ")
	fmt.Scan(&n4)
	media = (n1 + n2 + n3 + n4) / 4.0
	fmt.Printf("Sua média é de %.2f pontos", media)
}
