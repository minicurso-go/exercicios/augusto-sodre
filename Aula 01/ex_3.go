package main

import "fmt"

func main() {
	var base float32
	var altura float32
	var profundidade float32
	var volume float32
	fmt.Println("Calculadora de volume da caixa!")
	fmt.Print("Base da caixa: ")
	fmt.Scan(&base)
	fmt.Print("Altura da caixa: ")
	fmt.Scan(&altura)
	fmt.Print("Profundidade da caixa: ")
	fmt.Scan(&profundidade)
	volume = base * altura * profundidade
	fmt.Printf("Seu volume é de %.2fm³", volume)
}
