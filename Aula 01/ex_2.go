package main

import "fmt"

func main() {
	var length float32
	var height float32
	var area float32
	fmt.Println("Rectangle area calculator")
	fmt.Print("Give me the rectangle's height: ")
	fmt.Scan(&height)
	fmt.Print("Give me the rectangle's length: ")
	fmt.Scan(&length)
	area = height * length
	fmt.Println("Your rectangle has", area, "sqrm")
}
