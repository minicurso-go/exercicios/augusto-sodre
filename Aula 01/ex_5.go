package main

import "fmt"

func main() {
	var preco_dolar, quantia_depositada, quantia_convertida float32
	fmt.Println("Conversor de moedas!")
	fmt.Print("Dê-me o valor do dólar hoje: ")
	fmt.Scan(&preco_dolar)
	fmt.Print("Quantos reais serão depositados: ")
	fmt.Scan(&quantia_depositada)
	quantia_convertida = quantia_depositada / preco_dolar
	fmt.Printf("Você terá, ao final, %.2f dólares", quantia_convertida)
}
