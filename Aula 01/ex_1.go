package main

import "fmt"

func main() {
	var name string
	var age int
	var weight float32
	fmt.Print("Give me your name: ")
	fmt.Scan(&name)
	fmt.Print("Give me your age: ")
	fmt.Scan(&age)
	fmt.Print("Give me your weight (kg): ")
	fmt.Scan(&weight)
	fmt.Printf("Your name is %s, you are %d years old and you weight %.2fkg", name, age, weight)
}
