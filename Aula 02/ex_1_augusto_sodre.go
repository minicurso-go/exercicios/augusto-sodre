package main

import "fmt"

func main() {
	var x, y float32
	fmt.Print("Dê-me a coordenada X: ")
	fmt.Scan(&x)
	fmt.Print("Dê-me a coordenada Y: ")
	fmt.Scan(&y)
	if (x > 0) && (y > 0) {
		fmt.Println("Você está no primeiro quadrante!")
	} else if (x < 0) && (y > 0) {
		fmt.Println("Você está no segundo quadrante!")
	} else if (x < 0) && (y < 0) {
		fmt.Println("Você está no terceiro quadrante!")
	} else if (x > 0) && (y < 0) {
		fmt.Println("Você está no quarto quadrante!")
	} else if (x == 0) && (y != 0) {
		fmt.Println("Você está sob o eixo X!")
	} else if (x != 0) && (y == 0) {
		fmt.Println(("Você está sob o eixo Y!"))
	} else {
		fmt.Println("Você está na origem!")
	}
}
