package main

import "fmt"

func main() {
	var n1, n2, result float32
	var answer int
	fmt.Print("Dê-me um número: ")
	fmt.Scan(&n1)
	fmt.Print("Dê-me outro número: ")
	fmt.Scan(&n2)
	fmt.Printf("Opções de operação: \n1.Adição \n2.Subtração \n3.Multiplicação \n4.Divisão \n")
	fmt.Print("Opção [1/2/3/4]: ")
	fmt.Scan(&answer)
	switch answer {
	case 1:
		result = n1 + n2
		fmt.Printf("Resultado: %.2f", result)
	case 2:
		result = n1 - n2
		fmt.Printf("Resultado: %.2f", result)
	case 3:
		result = n1 * n2
		fmt.Printf("Resultado: %.2f", result)
	case 4:
		result = n1 / n2
		fmt.Printf("Resultado %.2f", result)
	}

}
