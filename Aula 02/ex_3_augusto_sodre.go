package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c float64
	const pi = 3.14159
	fmt.Printf("\nCalculadora de áreas!\n")
	fmt.Println("Área do triângulo retângulo!")
	fmt.Print("Dê-me o valor da base: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me o valor da altura: ")
	fmt.Scan(&c)
	fmt.Printf("Área: %.2fm²\n\n", (a*c)/2.0)
	fmt.Println("Área do círculo!")
	fmt.Print("Dê-me o valor do raio: ")
	fmt.Scan(&b)
	fmt.Printf("Área: %.2fm²\n\n", (pi * math.Pow(b, 2.0)))
	fmt.Println("Área do trapézio!")
	fmt.Print("Dê-me o valor da base menor: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me o valor da base maior: ")
	fmt.Scan(&b)
	fmt.Print("Dê-me o valor da altura: ")
	fmt.Scan(&c)
	fmt.Printf("Área: %.2fm²\n\n", (a+b)*c/2)
	fmt.Println("Área do quadrado!")
	fmt.Print("Dê-me o valor do lado: ")
	fmt.Scan(&b)
	fmt.Printf("Área: %.2fm²\n\n", math.Pow(b, 2.0))
	fmt.Println("Área do retângulo!")
	fmt.Print("Dê-me o valor da base: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me o valor da altura: ")
	fmt.Scan(&b)
	fmt.Printf("Área: %.2fm²\n\n", a*b)
}
