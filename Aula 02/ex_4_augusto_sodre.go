package main

import "fmt"

func main() {
	var a, b, c, d int
	fmt.Println("Calculadora da DIFERENÇA!")
	fmt.Print("Dê-me o primeiro número: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me o segundo número: ")
	fmt.Scan(&b)
	fmt.Print("Dê-me o terceiro número: ")
	fmt.Scan(&c)
	fmt.Print("Dê-me o quarto número: ")
	fmt.Scan(&d)
	fmt.Printf("A diferença entre o produto dos dois primeiros e o produto dos dois últimos é de %d", (a*b - c*d))
}
