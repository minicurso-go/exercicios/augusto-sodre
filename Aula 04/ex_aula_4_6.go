package main

import (
	"fmt"
	"math"
)

func potencia(base float64, exp float64) float64 {
	return math.Pow(base, exp)
}

func main() {
	var base, exp float64
	fmt.Println("Potencialização!")
	fmt.Print("Dê-me a base: ")
	fmt.Scan(&base)
	fmt.Print("Dê-me o expoente: ")
	fmt.Scan(&exp)
	fmt.Printf("O número %.2f elevado a %.2f resulta em %.2f", base, exp, potencia(base, exp))
}
