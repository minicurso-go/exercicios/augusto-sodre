package main

import "fmt"

func verificador(num int) bool {
	for i := num; i >= 2; i-- {
		if (num%i == 0) && (num != i) {
			return false
		}
	}
	return true
}

func main() {
	var num int
	fmt.Println("Verificador de primo!")
	fmt.Print("Dê-me o número a ser verificado: ")
	fmt.Scan(&num)
	fmt.Println("Número é primo:", verificador(num))
}
