package main

import "fmt"

func conversor(r int, c float32, f float32) float32 {
	if r == 1 {
		f = (1.8 * c) + 32.0
		return f
	} else {
		c = (f - 32) / 1.8
		return c
	}
}

func main() {
	var temp_cel, temp_fah float32
	var resp int
	fmt.Println("Conversor de temperaturas! Escolha uma opção:")
	fmt.Printf("1. Converter de Celcius para Fahrenheit \n2. Converter de Fahrenheit para Celcius\n")
	fmt.Print("Resposta: ")
	fmt.Scan(&resp)
	if resp == 1 {
		fmt.Print("Temperatura em graus Celcius: ")
		fmt.Scan(&temp_cel)
		temp_fah = 0
		fmt.Printf("Temperatura em graus Fahrenheit: %.2f°F", conversor(resp, temp_cel, temp_fah))
	} else if resp == 2 {
		fmt.Print("Temperatura em graus Fahrenheit: ")
		fmt.Scan(&temp_fah)
		temp_cel = 0
		fmt.Printf("Temperatura em graus Celcius: %.2f°C", conversor(resp, temp_cel, temp_fah))
	} else {
		fmt.Print("Resposta inexistente")
	}
}
