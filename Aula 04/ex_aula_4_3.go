package main

import "fmt"

func media(n1 float32, n2 float32, n3 float32, n4 float32) float32 {
	var soma = n1 + n2 + n3 + n4
	return soma / 4.0
}

func main() {
	var n1, n2, n3, n4 float32
	fmt.Println("Média de 4!")
	fmt.Print("Dê-me o primeiro valor: ")
	fmt.Scan(&n1)
	fmt.Print("Dê-me o segundo valor: ")
	fmt.Scan(&n2)
	fmt.Print("Dê-me o terceiro valor: ")
	fmt.Scan(&n3)
	fmt.Print("Dê-me o quarto valor: ")
	fmt.Scan(&n4)
	fmt.Printf("Sua média será de %.2f", media(n1, n2, n3, n4))

}
