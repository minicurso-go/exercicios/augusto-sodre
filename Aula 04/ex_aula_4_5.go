package main

import "fmt"

func busca(num int, tam_lista int, lista [6]int) (int, int) {
	for i := 0; i < tam_lista; i++ {
		if lista[i] == num {
			return num, i
		}
	}
	return 0, 0
}

func main() {
	var lista [6]int
	var num int
	fmt.Println("Buscador de elementos!")
	fmt.Println("Vamos criar nossa lista de 6 elementos!")
	for i := 0; i < 6; i++ {
		fmt.Print("Dê-me um número: ")
		fmt.Scan(&lista[i])
	}
	fmt.Print("Número que deseja buscar na lista: ")
	fmt.Scan(&num)
	var termoBuscado, posicao = busca(num, len(lista), lista)
	fmt.Printf("O termo buscado é %d e sua posição no vetor é %d", termoBuscado, posicao)

}
