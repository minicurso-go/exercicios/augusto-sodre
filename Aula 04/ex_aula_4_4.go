package main

import "fmt"

func fatorial(num int) int {
	valorFatorial := 1
	for i := num; i >= 1; i-- {
		valorFatorial = valorFatorial * i
	}
	return valorFatorial
}

func main() {
	var num int
	fmt.Println("Calculador de fatorial!")
	fmt.Print("Dê-me um número inteiro: ")
	fmt.Scan(&num)
	fmt.Printf("Seu fatorial é: %d", fatorial(num))

}
