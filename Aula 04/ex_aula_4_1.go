package main

import "fmt"

func soma(a int, b int) int {
	return a + b
}

func main() {
	var a, b int
	fmt.Print("Dê-me um número inteiro: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me outro número inteiro: ")
	fmt.Scan(&b)
	fmt.Println(soma(a, b))
}
