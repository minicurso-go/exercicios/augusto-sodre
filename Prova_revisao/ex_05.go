package main

import (
	"fmt"
)

func printPiramid(altura int) {
	if altura > 0 {
		//Primeiro for vai linha por linha
		for i := 1; i <= altura; i++ {
			//Segundo for calcula os espaços
			for j := 1; j <= altura-i; j++ {
				fmt.Print(" ")
			}
			//Terceiro for calcula os asteriscos
			for k := 1; k <= 2*i-1; k++ {
				fmt.Print("*")
			}
			//Pula Linha ao final da escrita da mesma
			fmt.Println()
		}
	}
}

func main() {
	var altura int
	fmt.Println("ESQUEMA DE PIRÂMIDE!")
	fmt.Print("Digite a altura da pirâmide: ")
	fmt.Scan(&altura)
	printPiramid(altura)

}
