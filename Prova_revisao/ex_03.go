package main

import "fmt"

func calcularDeterminante() {

}

func main() {
	var n int
	var matriz [][]int
	fmt.Print("Dê-me o valor de n da matriz n X n: ")
	fmt.Scan(&n)
	matriz = make([][]int, n) //Gera as colunas vazias
	for i := 0; i < n; i++ {
		matriz[i] = make([]int, n) //Gera as linhas vazias
		for j := 0; j < n; j++ {
			var resp int
			fmt.Printf("Valor da linha %d coluna %d: ", i, j)
			fmt.Scan(&resp)
			matriz[i][j] = resp
		}
	}

	fmt.Println(matriz)
}
