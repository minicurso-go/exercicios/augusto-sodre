package main

import (
	"fmt"
)

func main() {
	var num1, num2 int
	var numPrimos []int
	var respNum1, respNum2 int
	fmt.Print("Dê-me um número inicial: ")
	fmt.Scan(&num1)
	fmt.Print("Dê-me um número final: ")
	fmt.Scan(&num2)
	respNum1 = num1
	respNum2 = num2
	if num1 <= 0 {
		num1 = 2
	} else if num2 <= 0 {
		num2 = 2
	}

	/*
		Criar um array do tamanho do intervalo torna o código mais ineficiente, já que depois teríamos
		que tirar todos os valores vazios presentes em tal array usando outro loop de repetição (Afinal,
		não é porque existem 10 números entre 10 e 20 que os 10 serão primos!).

		var length float64
		length = math.Abs(float64(num1 - num2))
		fmt.Println(length)
		numPrimos = make([]int, int(length))
	*/

	for i := num1; i <= num2; i++ {
		contador := 0
		for j := 1; j <= i; j++ {
			if i%j == 0 && i != 1 {
				contador += 1
			}
		}
		if contador == 2 {
			numPrimos = append(numPrimos, i)
		}
	}
	fmt.Printf("Intervalo de [%d, %d]\n", respNum1, respNum2)
	fmt.Println("Primos:", numPrimos)
}
