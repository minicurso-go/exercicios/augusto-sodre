package main

import "fmt"

//Utilizei função recursiva, mas não fugi do if/else

func printNumber(num int, listaPares []int, listaImpares []int) {
	if num%2 == 0 {
		listaPares = append(listaPares, num)
	} else {
		listaImpares = append(listaImpares, num)
	}
	if num != 0 {
		printNumber(num-1, listaPares, listaImpares)
	} else {
		fmt.Println("Pares:", listaPares)
		fmt.Println("Impares:", listaImpares)
	}
}

func main() {
	var listaPares []int
	var listaImpares []int
	printNumber(10, listaPares, listaImpares)
}
