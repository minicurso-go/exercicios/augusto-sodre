package main

import "fmt"

func fatorial(num int, resultado int) int {
	if num != 1 && num != 0 {
		resultado *= num
		return fatorial(num-1, resultado)
	} else if num == 0 {
		return 1
	} else {
		return resultado
	}
}

func check(num int) int {
	if num < 0 {
		fmt.Print("Deu erro! Digite outro número: ")
		fmt.Scan(&num)
	}
	return num

}

func main() {
	var num int
	resultado := 1
	fmt.Println("FATORIAL!")
	fmt.Printf("Dê-me o número a analisar: ")
	fmt.Scan(&num)
	num = check(num)
	fmt.Println("Resultado: ", fatorial(num, resultado))
}
