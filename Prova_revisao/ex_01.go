package main

import (
	"fmt"
	"strconv"
	"strings"
)

func operations(num1 float64, num2 float64, operation string) (float64, error) {
	var result float64
	if operation == "+" {
		result = num1 + num2
	} else if operation == "-" {
		result = num1 - num2
	} else if operation == "*" {
		result = num1 * num2
	} else if operation == "/" {
		if num2 != 0 {
			result = num1 / num2
		} else {
			return result, fmt.Errorf("Não divida por zero!")
		}
	}
	return result, nil
}

func main() {
	var entrada string
	var num1, num2 string
	var operation string
	var operationIndex int
	fmt.Println("CALCULADORA DE EXPRESSÕES!")
	fmt.Print("Dê-me uma operação: ")
	fmt.Scan(&entrada)
	entrada = strings.TrimSpace(entrada)
	//Tratamento da entrada
	for i := 0; i < len(entrada); i++ {
		if string(entrada[i]) == "+" || string(entrada[i]) == "-" || string(entrada[i]) == "*" || string(entrada[i]) == "/" {
			operationIndex = i
		}
	}
	num1 = entrada[:operationIndex]
	num2 = entrada[operationIndex+1:]
	floatNum1, _ := strconv.ParseFloat(num1, 64)
	floatNum2, _ := strconv.ParseFloat(num2, 64)
	operation = entrada[operationIndex : operationIndex+1]
	fmt.Println(floatNum1, operation, floatNum2)
	result, err := operations(floatNum1, floatNum2, operation)
	if err == nil {
		fmt.Println("Resultado:", result)
	} else {
		fmt.Println(err)
	}
}
