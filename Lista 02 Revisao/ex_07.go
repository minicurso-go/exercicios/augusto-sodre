package main

import "fmt"

func main() {
	matriz := [2][3]int{{1, 2, 3}, {4, 5, 6}}
	var linha, coluna int
	fmt.Print("Dê-me o índice da linha desejada: ")
	fmt.Scan(&linha)
	fmt.Print("Dê-me o índice da coluna desejada: ")
	fmt.Scan(&coluna)
	fmt.Print("Elemento: ", matriz[linha][coluna])
}
