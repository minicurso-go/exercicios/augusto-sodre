package main

import (
	"fmt"
	"os"
)

type aluno struct {
	nome  string
	idade int
	notas []float64
}

func adicionarNotas(alunoLocal aluno) aluno {
	var notaLocal float64
	fmt.Println("Notas atuais: ", alunoLocal.notas)
	fmt.Print("Dê-me a nova nota: ")
	fmt.Scan(&notaLocal)
	alunoLocal.notas = append(alunoLocal.notas, notaLocal)
	return alunoLocal
}

func removerNotas(alunoLocal aluno) aluno {
	var index int
	fmt.Println("Notas atuais: ", alunoLocal.notas)
	fmt.Print("Posição da nota que deseja tirar: ")
	fmt.Scan(&index)
	alunoLocal.notas = append(alunoLocal.notas[:index], alunoLocal.notas[index+1:]...)
	return alunoLocal
}

func calcularMedia(alunoLocal aluno) float64 {
	var soma, media, contador float64
	soma = 0
	for i := range alunoLocal.notas {
		soma += alunoLocal.notas[i]
		contador = float64(i)
	}
	media = soma / (contador + 1.0)
	return media
}

func exibirFicha(alunoLocal aluno, media float64) {
	fmt.Println("Nome: ", alunoLocal.nome)
	fmt.Println("Idade: ", alunoLocal.idade)
	fmt.Printf("Média: %.2f\n", media)
}

func main() {
	var contadorNotas int
	var answer int
	var media float64
	newAluno := aluno{nome: "Agosto", idade: 0, notas: make([]float64, 0)}
	fmt.Println("Ficha cadastral!")
	fmt.Print("Nome do aluno: ")
	fmt.Scan(&newAluno.nome)
	fmt.Print("Idade do aluno: ")
	fmt.Scan(&newAluno.idade)
	fmt.Print("Quantas notas deseja adicionar: ")
	fmt.Scan(&contadorNotas)
	for i := 0; i < contadorNotas; i++ {
		var notaLocal float64
		fmt.Printf("Me dê a nota %d: ", i+1)
		fmt.Scan(&notaLocal)
		newAluno.notas = append(newAluno.notas, notaLocal)
	}
	for answer != 5 {
		fmt.Printf("\nE agora?\n1. Adicionar mais notas \n2. Remover notas \n3. Calcular média \n4. Exibir Ficha \n5. Sair do programa \nResposta: ")
		fmt.Scan(&answer)
		switch answer {
		case 1:
			fmt.Println()
			newAluno = adicionarNotas(newAluno)
		case 2:
			fmt.Println()
			newAluno = removerNotas(newAluno)
		case 3:
			fmt.Println()
			media = calcularMedia(newAluno)
			fmt.Printf("Média: %.2f\n", media)
		case 4:
			fmt.Println()
			media = calcularMedia(newAluno)
			exibirFicha(newAluno, media)
		case 5:
			os.Exit(0)
		default:
			fmt.Println("Erro! Digite um número existente!")
		}
	}

}
