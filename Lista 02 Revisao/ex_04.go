package main

import "fmt"

type viagem struct {
	origem  string
	destino string
	data    string
	preco   float32
}

func comparadorViagemCara(listaViagens []viagem) viagem {
	var viagemCara viagem
	viagemCara.preco = -10000000
	for i := 0; i < len(listaViagens); i++ {
		if listaViagens[i].preco > viagemCara.preco {
			viagemCara = listaViagens[i]
		}
	}
	return viagemCara
}

func main() {
	var viagens []viagem
	var newViagem viagem
	var answer string

	for answer != "n" {
		var origemLocal, destinoLocal, dataLocal string
		var precoLocal float32
		fmt.Printf("\nNova viagem!\n")
		fmt.Print("Origem da viagem: ")
		fmt.Scan(&origemLocal)
		fmt.Print("Destino da viagem: ")
		fmt.Scan(&destinoLocal)
		fmt.Print("Data da viagem [dd/mm/aaaa]: ")
		fmt.Scan(&dataLocal)
		fmt.Print("Preço da viagem: ")
		fmt.Scan(&precoLocal)
		newViagem = viagem{origem: origemLocal, destino: destinoLocal, data: dataLocal, preco: precoLocal}
		viagens = append(viagens, newViagem)
		fmt.Print("Deseja criar uma nova viagem? [s/n]: ")
		fmt.Scan(&answer)
	}

	fmt.Println("Viagem mais cara:", comparadorViagemCara(viagens))

}
