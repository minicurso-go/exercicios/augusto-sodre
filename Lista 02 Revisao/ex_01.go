package main

import "fmt"

type Triangle struct {
	base   float32
	altura float32
}

func areaTriangulo(triangle Triangle) {
	area := triangle.base * triangle.altura / 2.0
	fmt.Println("Área do triângulo:", area)
}

func main() {
	var base, altura float32
	fmt.Println("Calculadora da área de um triângulo!")
	fmt.Print("Dê-me a base do triângulo: ")
	fmt.Scan(&base)
	fmt.Print("Dê-me a altura do triângulo: ")
	fmt.Scan(&altura)
	areaTriangulo(Triangle{base: base, altura: altura})
}
