package main

import "fmt"

func main() {
	var matriz [3][2]int

	fmt.Println("Dê-me os elementos da matriz, linha por linha:")

	for i := 0; i < 3; i++ {
		for j := 0; j < 2; j++ {
			fmt.Scan(&matriz[i][j])
		}
	}

	for i := 0; i < 3; i++ {
		fmt.Print("[")
		for j := 0; j < 2; j++ {
			fmt.Print(" ", matriz[i][j], " ")
		}
		fmt.Println("]")
	}
}
