package main

import "fmt"

func main() {
	mapinha1 := map[string]int{"eu": 2, "o": 6, "uau": 2}
	mapinha2 := map[string]int{"a": 6, "o": 2, "nossa": 1}
	mapinha3 := map[string]int{"a": 6, "o": 2, "nossa": 1}
	listaMapinhas := []map[string]int{mapinha1, mapinha2, mapinha3}
	var mapaFinal map[string]int
	listaMapinhas = append(listaMapinhas, mapinha1)
	listaMapinhas = append(listaMapinhas, mapinha2)
	listaMapinhas = append(listaMapinhas, mapinha3)
	mapaFinal = make(map[string]int)
	for i := 1; i < len(listaMapinhas); i++ {
		for chave1, valor1 := range listaMapinhas[i-1] {
			for chave2, valor2 := range listaMapinhas[i] {
				if chave1 != chave2 {
					mapaFinal[chave1] = valor1
					mapaFinal[chave2] = valor2
				} else {
					mapaFinal[chave1] += valor1
				}
			}
		}

	}
	fmt.Println(mapaFinal)

}