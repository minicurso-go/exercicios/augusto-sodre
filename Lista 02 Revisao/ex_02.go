package main

import "fmt"

type funcionario struct {
	nome    string
	salario float32
	idade   int
}

func tempoServico(funcionario funcionario) int {
	return funcionario.idade - 18
}

func ajusteSalarial(funcionario funcionario, ajuste float32) float32 {
	return funcionario.salario * (1 + ajuste)
}

func main() {
	var nomeLocal string
	var salarioLocal float32
	var idadeLocal int
	var ajuste float32
	fmt.Print("Dê-me o nome do funcionário: ")
	fmt.Scan(&nomeLocal)
	fmt.Print("Dê-me o salário atual do funcionário: ")
	fmt.Scan(&salarioLocal)
	fmt.Print("Dê-me a idade do funcionário: ")
	fmt.Scan(&idadeLocal)
	var newFuncionario = funcionario{nome: nomeLocal, salario: salarioLocal, idade: idadeLocal}
	fmt.Println("Tempo de serviço:", tempoServico(newFuncionario))
	fmt.Print("Ajuste salarial em porcentagem (Positivo ou negativo): ")
	fmt.Scan(&ajuste)
	fmt.Println("Novo salário:", ajusteSalarial(newFuncionario, ajuste/100))

}
