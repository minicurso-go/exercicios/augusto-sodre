package main

import "fmt"

type animal struct {
	nome    string
	especie string
	idade   int
	som     string
}

func trocarSom(animal2 animal) string {
	var somLocal string
	fmt.Print("Novo som do animal: ")
	fmt.Scan(&somLocal)
	return somLocal
}

func mostrarFicha(animal2 animal) {
	fmt.Printf("\nFICHA CADASTRAL\n")
	fmt.Println("Nome:", animal2.nome)
	fmt.Println("Espécie:", animal2.especie)
	fmt.Println("Idade:", animal2.idade)
	fmt.Println("Som:", animal2.som)
}

func main() {
	var answer string
	var nomeLocal, especieLocal, somLocal string
	var idadeLocal int
	var newAnimal animal
	fmt.Println("Ficha cadastral animal")
	fmt.Print("Dê-me o nome do animal: ")
	fmt.Scan(&nomeLocal)
	fmt.Print("Dê-me a espécie do animal: ")
	fmt.Scan(&especieLocal)
	fmt.Print("Dê-me a idade do animal: ")
	fmt.Scan(&idadeLocal)
	fmt.Print("Dê-me o som do animal: ")
	fmt.Scan(&somLocal)
	newAnimal = animal{nome: nomeLocal, especie: especieLocal, idade: idadeLocal, som: somLocal}
	fmt.Print("Deseja alterar o som do animal? [s/n]: ")
	fmt.Scan(&answer)
	if answer == "s" {
		newAnimal = animal{nome: nomeLocal, especie: especieLocal, idade: idadeLocal, som: trocarSom(newAnimal)}
	}
	mostrarFicha(newAnimal)
}
