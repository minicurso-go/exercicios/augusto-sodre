package main

import "fmt"

func merge(map1 map[string]int, map2 map[string]int) map[string]int {
	var mapFinal map[string]int
	mapFinal = make(map[string]int)
	for chave1, valor1 := range map1 {
		for chave2, valor2 := range map2 {
			if chave1 != chave2 {
				mapFinal[chave1] = valor1
				mapFinal[chave2] = valor2
			} else {
				mapFinal[chave2] = valor2
			}
		}
	}
	return mapFinal
}

func main() {
	map1 := map[string]int{"Dia": 02, "Mes": 12, "Ano": 2024}
	map2 := map[string]int{"Temperatura": 28, "PrecoAgua": 12, "Amigos": 0, "Ano": 1999}
	fmt.Println(merge(map1, map2))
}
