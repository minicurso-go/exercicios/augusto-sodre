package main

import "fmt"

func somador(mapa map[string]int) int {
	var soma int
	for _, valor := range mapa {
		soma += valor
	}
	return soma
}

func main() {
	mapinha := map[string]int{"Valor 01": 12, "Valor 02": 16, "valor 03": 20, "Valor 04": 24, "Valor 05": 28}
	fmt.Println(somador(mapinha))
}
