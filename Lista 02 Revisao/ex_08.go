package main

import (
	"fmt"
	"strings"
)

func contadorMapeador(listaPalavras []string) map[string]int {
	var contadorLocal map[string]int
	contadorLocal = make(map[string]int)
	for i := 0; i < len(listaPalavras); i++ {
		contadorLocal[listaPalavras[i]] += 1
	}
	return contadorLocal
}

func main() {
	var trecho = "Doze vozes gritavam, cheias de ódio, e eram todos iguais. Não havia dúvida, agora, quanto ao que sucedera à fisionomia dos porcos. As criaturas de fora olhavam de um porco para um homem, de um homem para um porco e de um porco para um homem outra vez; mas já se tornara impossível distinguir, quem era homem, quem era porco."
	var palavras []string
	trecho = strings.Trim(trecho, ",;.")
	palavras = strings.Split(trecho, " ")
	fmt.Println(contadorMapeador(palavras))
}
